<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'anons')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'img')->fileInput(['maxlength' => true]) ?>

    <?
    // получаем всех пользователей
    $users = \app\models\Users::find()->all();
    // формируем массив, с ключем равным полю 'id' и значением равным полю 'name'
    $items = ArrayHelper::map($users,'id','username');
    ?>
    <?= $form->field($model, 'user_id')->dropDownList($items) ?>

    <?php $model->newsTopics =  ArrayHelper::getColumn($model->newsTopics, 'id');?>

    <?
    // получаем все темы
    $topics = \app\models\Topics::find()->all();
    // формируем массив, с ключем равным полю 'id' и значением равным полю 'title'
    $items = ArrayHelper::map($topics,'id','title');
    ?>
    <?= $form->field($model, 'newsTopics')->dropDownList($items,['multiple'=>'true']) ?>
    <?= $form->field($model, 'topics_list')
        ->dropDownList(ArrayHelper::map(\app\models\Topics::find()->all(), 'id', 'name'), ['multiple' => true]) ?>

    <?= $form->field($model, 'update_date')->widget(
        DatePicker::className(),
        [
            'clientOptions' => ['periodfrom' => '1980-01-01'],
            'dateFormat' => 'dd.MM.yyyy',
            'language' =>'RU',

        ]
    ) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
