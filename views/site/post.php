<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/post-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <?= $model->title ?>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <!-- Post Content -->
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <?= nl2br($model->anons) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 img-responsive">
            <br>
            <?= $model->content ?>
        </div>
    </div>
    <footer>
        <p class="post-meta">
            Запостил <a href="#"><?= $model->author->nickname ?></a>
            от <?= Yii::$app->formatter->asDate($model->publish_date, 'd.M.Y') . ' г.' ?>
        </p>
    </footer>
</div>