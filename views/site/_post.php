<?php
/**
 * Created by PhpStorm.
 * User: 2013
 * Date: 02.12.2015
 * Time: 13:11
 */
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

Yii::$app->formatter->locale = 'ru-RU';
?>
<div class="article">
    <?php echo Html::a(//сылка
        '<h2 class="post-preview">'.$news->title.'</h2>',//текст ссылки
        [
            'post',//адрес ссылки
            'id' => $news->id,//параметр(ы) ссылки
        ]
    );
    ?>

    <h3 class="post-subtitle" data-id="<?= $news->id ?>">
        <?= HtmlPurifier::process($news->anons) ?>
    </h3>

    <p class="post-meta">
        Запостил <a href="#"><?= $news->user->username ?></a>
        от <?= Yii::$app->formatter->asDate($news->update_date,'d.M.Y').' г.' ?>
    </p>

</div>
<hr>