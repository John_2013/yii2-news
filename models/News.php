<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $anons
 * @property string $img
 * @property integer $user_id
 * @property string $update_date
 *
 * @property Users $user
 * @property NewsTopics[] $newsTopics
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * Список категорий
     * @var array
     */
    public $topics_list = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'anons'], 'required'],
            [['anons'], 'string'],
            [['user_id'], 'integer'],
            [['update_date','topics_list'], 'safe'],
            [['title', 'img'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'заголовок',
            'anons' => 'текст',
            'img' => 'картинка',
            'user_id' => 'User ID',
            'update_date' => 'дата изменения',
            'newsTopics' =>'Темы'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsTopics()
    {
        return $this->hasMany(NewsTopics::className(), ['news_id' => 'id']);
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // сбрасываем список категорий
        (new Query())
            ->createCommand()
            ->delete('{{%newsTopics}}', 'news_id=:id', [':id' => $this->id])
            ->execute();

        // сохраняем список категорий
        if ($this->topics_list) {

            foreach ($this->topics_list as $k => $v) {
                $model = NewsTopics::find()->where('id=:id', [':id' => $v])->one();
                if ($model)
                    $this->link('newsTopics', $model);
            }
        }
        // Yii::$app->cache->delete($this->getCacheKey());
    }
}
