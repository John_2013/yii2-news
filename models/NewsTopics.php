<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_topics".
 *
 * @property integer $news_id
 * @property integer $topic_id
 *
 * @property News $news
 * @property Topics $topic
 */
class NewsTopics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_topics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'topic_id'], 'required'],
            [['news_id', 'topic_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'новость',
            'topic_id' => 'тема',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(Topics::className(), ['id' => 'topic_id']);
    }
}
