<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "topics".
 *
 * @property integer $id
 * @property string $title
 *
 * @property NewsTopics[] $newsTopics
 */
class Topics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'тема',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsTopics()
    {
        return $this->hasMany(NewsTopics::className(), ['topic_id' => 'id']);
    }
}
