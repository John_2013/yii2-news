<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_084911_dbNews extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%topics}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL COMMENT \'тема\'',
        ], $tableOptions);

        $this->createTable('{{%news}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL COMMENT \'заголовок\'',
            'anons' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'текст\'',
            'img' => Schema::TYPE_STRING.' NULL COMMENT \'картинка\'',
            'user_id' => Schema::TYPE_INTEGER,
            'update_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT \'дата изменения\'',
        ], $tableOptions);

        $this->createTable('{{%news_topics}}', [ 'news_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT \'новость\'',
            'topic_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT \'тема\'',
            'PRIMARY KEY `id` (news_id, topic_id)',
        ], $tableOptions);

        $this->createTable('{{%users}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . ' NOT NULL COMMENT \'имя\'',
            'password' => Schema::TYPE_STRING . ' NOT NULL COMMENT \'тема\'',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'date_of_create' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'дата регистрации\'',
            'admin' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0 COMMENT \'права администратора\'',
        ], $tableOptions);

        $this->createIndex('FK_news_user_id', '{{%news}}', 'user_id');
        $this->addForeignKey(
            'FK_news_user_id', '{{%news}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE'
        );

        $this->createIndex('FK_news-topics_news_id', '{{%news_topics}}', 'news_id');
        $this->createIndex('FK_news-topics_topic_id', '{{%news_topics}}', 'topic_id');
        $this->addForeignKey(
            'FK_news-topics_news_id', '{{%news_topics}}', 'news_id', '{{%news}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'FK_news-topics_topic_id', '{{%news_topics}}', 'topic_id', '{{%topics}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%news}}');
        $this->dropTable('{{%topics}}');
        $this->dropTable('{{%news_topics}}');
        $this->dropTable('{{%users}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
